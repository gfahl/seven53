#!/usr/bin/ruby
require 'minitest/autorun'
require './row'

class TestRow < MiniTest::Unit::TestCase

  puts ("\n" + "=" * 10 + " %s " + "=" * 10) % self

  def setup
    Row.clear
  end

  def test_row_creation
    row = Row.get(5)
    assert_equal(5, row.width)
  end

  def test_row_new_private
    assert_raises(NoMethodError) do
      row = Row.new(5)
    end
  end

  def test_row_object_reuse
    r1 = Row.get(5)
    r2 = Row.get(5)
    r3 = Row.get(5)
    r4 = Row.get(17)
    r5 = Row.get(17)
    r6 = Row.get(42)
    assert_equal(r1, r2)
    assert_equal(r1, r3)
    refute_equal(r1, r4)
    refute_equal(r1, r5)
    refute_equal(r1, r6)
    assert_equal(r4, r5)
    assert_equal([5, 17, 42].sort, Row.all.map(&:width).sort)
  end

  def test_next_rows_1
    row = Row.get(1)
    row_sets = row.next_rows
    assert_equal(
      [[]].sort,
      row_sets.map { |rows| rows.map { |row| row.width }.sort }.sort
      )
  end

  def test_next_rows_2
    row = Row.get(2)
    row_sets = row.next_rows
    assert_equal(
      [[], [1]].sort,
      row_sets.map { |rows| rows.map { |row| row.width }.sort }.sort
      )
  end

  def test_next_rows_3
    row = Row.get(3)
    row_sets = row.next_rows
    assert_equal(
      [[], [1], [2], [1, 1]].sort,
      row_sets.map { |rows| rows.map { |row| row.width }.sort }.sort
      )
  end

  def test_next_rows_4
    row = Row.get(4)
    row_sets = row.next_rows
    assert_equal(
      [[], [1], [2], [3], [1, 1], [1, 2]].sort,
      row_sets.map { |rows| rows.map { |row| row.width }.sort }.sort
      )
  end

  def test_next_rows_5
    row = Row.get(5)
    row_sets = row.next_rows
    assert_equal(
      [[], [1], [2], [3], [4], [1, 1], [1, 2], [1, 3], [2, 2]].sort,
      row_sets.map { |rows| rows.map { |row| row.width }.sort }.sort
      )
  end

  def test_next_rows_10
    row = Row.get(10)
    row_sets = row.next_rows
    assert_equal(
      [[], [1], [2], [3], [4], [5], [6], [7], [8], [9],
       [1, 1], [1, 2], [1, 3], [1, 4], [1, 5], [1, 6], [1, 7], [1, 8],
       [2, 2], [2, 3], [2, 4], [2, 5], [2, 6], [2, 7],
       [3, 3], [3, 4], [3, 5], [3, 6],
       [4, 4], [4, 5]].sort,
      row_sets.map { |rows| rows.map { |row| row.width }.sort }.sort
      )
  end

end
