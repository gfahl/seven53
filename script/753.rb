require './board'

# Parse command-line arguments
$stderr.puts(ARGV.inspect) if $DEBUG
row_widths = []
ARGV.each do |x|
  row_widths << x.to_i if x =~ /^[0-9]+$/
end
$stderr.puts(row_widths.inspect) if $DEBUG

board = Board.get(*row_widths)

puts "\n%s is a %s position" % [board, board.is_winning_position ? "winning" : "losing"]
puts "\nLosing positions for this board:"
board.losing_positions.sort_by { |b| b.to_a.reverse }.reverse.each { |board| p board }
