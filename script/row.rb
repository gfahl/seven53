class Row

  attr_reader :width

  @@rows = {} # row width => Row object

  def Row.all; @@rows.values; end

  def Row.clear; @@rows = {}; end

  def Row.get(width)
    @@rows[width] ||= new(width)
  end

  private_class_method :new

  def initialize(width)
    @width = width
  end

  def next_rows
    # all sets of rows that can be created by stroking this row
    @next_rows_cache ||=
      if @width == 1 then [[]]
      else
        Row.get(@width - 1).next_rows +
          [[Row.get(@width - 1)]] +
          (1..(@width - 1) / 2).map { |i| [Row.get(i), Row.get(@width - i - 1)] }
      end
  end

end
