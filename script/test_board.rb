#!/usr/bin/ruby
require 'minitest/autorun'
require './board'

class TestBoard < MiniTest::Unit::TestCase

  puts ("\n" + "=" * 10 + " %s " + "=" * 10) % self

  def setup
    Board.clear
  end

  # ----------------------------------------

  def test_board_creation_empty
    board = Board.get()
    assert_equal(0, board.rows.size)
  end

  def test_board_creation_7_5_3
    board = Board.get(7, 5, 3)
    assert_equal(3, board.rows.size)
    assert_equal([3, 5, 7], board.rows.map(&:width))
  end

  def test_board_new_private
    assert_raises(NoMethodError) do
      board = Board.new(5)
    end
  end

  def test_board_object_reuse
    b1 = Board.get(7, 5, 3)
    b2 = Board.get(7, 5, 3)
    b3 = Board.get(3, 7, 5)
    b4 = Board.get(17, 42)
    b5 = Board.get(17, 42)
    b6 = Board.get(7)
    assert_equal(b1, b2)
    assert_equal(b1, b3)
    refute_equal(b1, b4)
    refute_equal(b1, b5)
    refute_equal(b1, b6)
    assert_equal(b4, b5)
    assert_equal([[3, 5, 7], [17, 42], [7]].sort, Board.all.map(&:to_a).sort)
  end

  # ----------------------------------------

  def test_to_a_1
    board = Board.get(1)
    assert_equal([1], board.to_a)
  end

  def test_to_a_7_5_3
    board = Board.get(7, 5, 3)
    assert_equal([3, 5, 7], board.to_a)
  end

  def test_to_a_3_7_5
    board = Board.get(3, 7, 5)
    assert_equal([3, 5, 7], board.to_a)
  end

  # ----------------------------------------

  def test_to_s_1
    board = Board.get(1)
    assert_equal("[1]", board.to_s)
  end

  def test_to_s_7_5_3
    board = Board.get(7, 5, 3)
    assert_equal("[7, 5, 3]", board.to_s)
  end

  def test_to_s_3_7_5
    board = Board.get(3, 7, 5)
    assert_equal("[7, 5, 3]", board.to_s)
  end

  # ----------------------------------------

  def test_next_boards_1
    board = Board.get(1)
    expected = [Board.get()]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_2
    board = Board.get(2)
    expected = [Board.get(), Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_3
    board = Board.get(3)
    expected = [Board.get(), Board.get(1), Board.get(2), Board.get(1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_4
    board = Board.get(4)
    expected = [Board.get(), Board.get(1), Board.get(2), Board.get(3), Board.get(1, 1), Board.get(1, 2)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_5
    board = Board.get(5)
    expected = [Board.get(), Board.get(1), Board.get(2), Board.get(3), Board.get(4),
      Board.get(1, 1), Board.get(1, 2), Board.get(1, 3), Board.get(2, 2)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_1_1
    board = Board.get(1, 1)
    expected = [Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_1_2
    board = Board.get(1, 2)
    expected = [Board.get(1), Board.get(2), Board.get(1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_1_3
    board = Board.get(1, 3)
    expected = [Board.get(1), Board.get(3), Board.get(1, 1), Board.get(1, 2), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_1_4
    board = Board.get(1, 4)
    expected = [
      Board.get(1), Board.get(4),
      Board.get(1, 1), Board.get(1, 2), Board.get(1, 3),
      Board.get(1, 1, 1), Board.get(1, 1, 2)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_1_5
    board = Board.get(1, 5)
    expected = [
      Board.get(1), Board.get(5),
      Board.get(1, 1), Board.get(1, 2), Board.get(1, 3), Board.get(1, 4),
      Board.get(1, 1, 1), Board.get(1, 1, 2), Board.get(1, 1, 3), Board.get(1, 2, 2)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_2_2
    board = Board.get(2, 2)
    expected = [Board.get(2), Board.get(1, 2)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_2_3
    board = Board.get(2, 3)
    expected = [
      Board.get(2), Board.get(3),
      Board.get(1, 2), Board.get(1, 3), Board.get(2, 2),
      Board.get(1, 1, 2)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_3_3
    board = Board.get(3, 3)
    expected = [
      Board.get(3),
      Board.get(1, 3), Board.get(2, 3),
      Board.get(1, 1, 3)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_1_1_1
    board = Board.get(1, 1, 1)
    expected = [Board.get(1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_1_1_2
    board = Board.get(1, 1, 2)
    expected = [Board.get(1, 1), Board.get(1, 2), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  def test_next_boards_1_1_1_1_1_1
    board = Board.get(1, 1, 1, 1, 1, 1)
    expected = [Board.get(1, 1, 1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.next_boards.map(&:to_a).sort
      )
  end

  # ----------------------------------------

  def test_all_boards_1
    board = Board.get(1)
    expected = [Board.get()]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_2
    board = Board.get(2)
    expected = [Board.get(), Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_3
    board = Board.get(3)
    expected = [Board.get(), Board.get(1), Board.get(2), Board.get(1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_4
    board = Board.get(4)
    expected = [
      Board.get(), Board.get(1), Board.get(2), Board.get(3), Board.get(1, 1), Board.get(1, 2)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_5
    board = Board.get(5)
    expected = [
      Board.get(), Board.get(1), Board.get(2), Board.get(3), Board.get(4),
      Board.get(1, 1), Board.get(1, 2), Board.get(1, 3), Board.get(2, 2),
      Board.get(1, 1, 1)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_1_1
    board = Board.get(1, 1)
    expected = [Board.get(), Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_1_2
    board = Board.get(1, 2)
    expected = [Board.get(), Board.get(1), Board.get(2), Board.get(1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_1_3
    board = Board.get(1, 3)
    expected = [
      Board.get(), Board.get(1), Board.get(2), Board.get(3),
      Board.get(1, 1), Board.get(1, 2),
      Board.get(1, 1, 1)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_1_4
    board = Board.get(1, 4)
    expected = [
      Board.get(), Board.get(1), Board.get(2), Board.get(3), Board.get(4),
      Board.get(1, 1), Board.get(1, 2), Board.get(1, 3),
      Board.get(1, 1, 1), Board.get(1, 1, 2)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_1_5
    board = Board.get(1, 5)
    expected = [
      Board.get(), Board.get(1), Board.get(2), Board.get(3), Board.get(4), Board.get(5),
      Board.get(1, 1), Board.get(1, 2), Board.get(1, 3), Board.get(1, 4), Board.get(2, 2),
      Board.get(1, 1, 1), Board.get(1, 1, 2), Board.get(1, 1, 3), Board.get(1, 2, 2),
      Board.get(1, 1, 1, 1)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_2_2
    board = Board.get(2, 2)
    expected = [
      Board.get(), Board.get(1), Board.get(2),
      Board.get(1, 1), Board.get(1, 2)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_2_3
    board = Board.get(2, 3)
    expected = [
      Board.get(), Board.get(1), Board.get(2), Board.get(3),
      Board.get(1, 1), Board.get(1, 2), Board.get(1, 3), Board.get(2, 2),
      Board.get(1, 1, 1), Board.get(1, 1, 2)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_3_3
    board = Board.get(3, 3)
    expected = [
      Board.get(), Board.get(1), Board.get(2), Board.get(3),
      Board.get(1, 1), Board.get(1, 2), Board.get(1, 3), Board.get(2, 2), Board.get(2, 3),
      Board.get(1, 1, 1), Board.get(1, 1, 2), Board.get(1, 1, 3),
      Board.get(1, 1, 1, 1)
    ]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_1_1_1
    board = Board.get(1, 1, 1)
    expected = [Board.get(), Board.get(1), Board.get(1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_1_1_2
    board = Board.get(1, 1, 2)
    expected = [Board.get(), Board.get(1), Board.get(2), Board.get(1, 1),
      Board.get(1, 2), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  def test_all_boards_1_1_1_1_1_1
    board = Board.get(1, 1, 1, 1, 1, 1)
    expected = [Board.get(), Board.get(1), Board.get(1, 1), Board.get(1, 1, 1),
      Board.get(1, 1, 1, 1), Board.get(1, 1, 1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.all_boards.map(&:to_a).sort
      )
  end

  # ----------------------------------------

  def test_is_winning_position_1
    board = Board.get(1)
    assert_equal(false, board.is_winning_position)
  end

  def test_is_winning_position_2
    board = Board.get(2)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_3
    board = Board.get(3)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_4
    board = Board.get(4)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_1_1
    board = Board.get(1, 1)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_1_2
    board = Board.get(1, 2)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_1_3
    board = Board.get(1, 3)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_2_2
    board = Board.get(2, 2)
    assert_equal(false, board.is_winning_position)
  end

  def test_is_winning_position_2_3
    board = Board.get(2, 3)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_2_4
    board = Board.get(2, 4)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_3_3
    board = Board.get(3, 3)
    assert_equal(false, board.is_winning_position)
  end

  def test_is_winning_position_3_4
    board = Board.get(3, 4)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_4_4
    board = Board.get(4, 4)
    assert_equal(false, board.is_winning_position)
  end

  def test_is_winning_position_1_1_1
    board = Board.get(1, 1, 1)
    assert_equal(false, board.is_winning_position)
  end

  def test_is_winning_position_1_1_2
    board = Board.get(1, 1, 2)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_1_1_3
    board = Board.get(1, 1, 3)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_1_1_1_1
    board = Board.get(1, 1, 1, 1)
    assert_equal(true, board.is_winning_position)
  end

  def test_is_winning_position_1_1_1_1_1
    board = Board.get(1, 1, 1, 1, 1)
    assert_equal(false, board.is_winning_position)
  end

  # ----------------------------------------

  def test_losing_positions_1
    board = Board.get(1)
    expected = [Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_2
    board = Board.get(2)
    expected = [Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_3
    board = Board.get(3)
    expected = [Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_1_1
    board = Board.get(1, 1)
    expected = [Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_1_2
    board = Board.get(1, 2)
    expected = [Board.get(1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_1_3
    board = Board.get(1, 3)
    expected = [Board.get(1), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_2_2
    board = Board.get(2, 2)
    expected = [Board.get(1), Board.get(2, 2)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_2_3
    board = Board.get(2, 3)
    expected = [Board.get(1), Board.get(2, 2), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_2_4
    board = Board.get(2, 4)
    expected = [Board.get(1), Board.get(2, 2), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_3_3
    board = Board.get(3, 3)
    expected = [Board.get(1), Board.get(2, 2), Board.get(3, 3), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_1_1_1
    board = Board.get(1, 1, 1)
    expected = [Board.get(1), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_1_1_2
    board = Board.get(1, 1, 2)
    expected = [Board.get(1), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_1_1_1_1
    board = Board.get(1, 1, 1, 1)
    expected = [Board.get(1), Board.get(1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

  def test_losing_positions_1_1_1_1_1
    board = Board.get(1, 1, 1, 1, 1)
    expected = [Board.get(1), Board.get(1, 1, 1), Board.get(1, 1, 1, 1, 1)]
    assert_equal(
      expected.map(&:to_a).sort,
      board.losing_positions.map(&:to_a).sort
      )
  end

end
