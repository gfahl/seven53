require './row'

class Board

  attr_reader :rows

  @@boards = {} # array of row widths (sorted) => Board object

  def Board.all; @@boards.values; end

  def Board.clear; @@boards = {}; end

  def Board.get(*row_widths)
    @@boards[row_widths.sort] ||= new(*row_widths)
  end

  private_class_method :new

  def initialize(*row_widths)
    @rows = row_widths.sort.map { |width| Row.get(width) }
  end

  def is_losing_position; !is_winning_position; end

  def is_winning_position
    # a board is a WINNING position if:
    # there exists a move which leads to a LOSING position
    @is_winning_position_cache ||=
      if @rows == [] then true
      else
        !!next_boards.find { |board| board.is_losing_position }
      end
  end

  def losing_positions
    # all boards that can be reached from this board (recursively) which are losing positions
    @losing_positions_cache ||=
      (all_boards << self).find_all { |board| board.is_losing_position }
  end

  def next_boards
    # all boards that can be created by one legal move on this board
    @next_boards_cache ||= begin
      res = []
      @rows.each_with_index do |row, i|
        other_rows = @rows.clone
        other_rows.delete_at(i)
        row.next_rows.each do |new_rows|
          res << Board.get(*(new_rows + other_rows).map(&:width))
        end
      end
      res.uniq
    end
  end

  def all_boards
    # all boards that can be reached from this board (recursively)
    @all_boards_cache ||= begin
      res = next_boards
      res.each do |board|
        res += board.all_boards
      end
      res.uniq
    end
  end

  def to_a
    @rows.map(&:width)
  end

  def to_s
    to_a.reverse.to_s
  end

end
