================================================================================
Project:    seven53
================================================================================

Purpose: set up TDD environment

--------------------------------------------------------------------------------
x
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Appendix
--------------------------------------------------------------------------------

chmod +x test.bash
chmod +x test_board.rb
chmod +x test_row.rb

./test.bash

Note: Current version of minitest is 5.4.1 but the one installed on my computer is 1.6.0
	1.9.2-p320 :002 > require 'minitest/unit'
	 => true 
	1.9.2-p320 :003 > MiniTest::Unit::VERSION
	 => "1.6.0" 

	 Use doc from "http://ruby-doc.org/stdlib-1.9.2/libdoc/minitest/unit/rdoc/index.html"

